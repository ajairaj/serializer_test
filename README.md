README
--------------------------------------------------------------------------------------------------------------------------------------------
STRUCTURE

There's 4 projects.
	1. serial_lib is the c++ library I wrote to serialize and deserialze directories
	2. Serializer_Test is a c++ application that runs through the test, as explained below
	3. serial_lib_cli is a CLR wrapper for serial_lib
	4. Serializer_Test_CS is a C# application, that does the same thing as Serializer_Test, but using the CLR wrapped library

--------------------------------------------------------------------------------------------------------------------------------------------
SOLUTION

This whole thing's built in VS2015.

It depends on the boost library. It's included in the packages.config files, so NuGet should fetch it automatically.
If you're using a different version of Visual Studio, you'll need to change the dependency from libboost VC-140 to whatever you're using

--------------------------------------------------------------------------------------------------------------------------------------------
DATA

2 directories are used to test the serializer
/testdata/64/ contains 4 files of 16 MB each
/testdata/256 contains 16 files of 16 MB each

--------------------------------------------------------------------------------------------------------------------------------------------
TEST PROCEDURE

So this is, in a nutshell, what the two test applications do.

	1. Serialize the two test directories. Files are loaded in to an std::map object. The map is serialized and saved into the test directories. 
	The serialized versions are stored in the following four files. The filename represents whether the c++ or cli application did the serializing
		a. /testdata/64/out_cpp.srl
		b. /testdata/64/out_cli.srl
		c. /testdata/256/out_cpp.srl
		d. /testdata/256/out_cli.srl
	2. (optional) Load and deserialze the std::map object, and store it in a private member of the Foo Class. More on why I did this later. 
	3. Load and deserialize the map object. Verify that deserialization works at all.
	4. Byte-comparison of the deserialized object and the files in the test folder. 
	5. Verify that the correct number of files are present in the std::map (4 and 16)
	6. Repeat for each test directory. Repeat again 1000 times. 

--------------------------------------------------------------------------------------------------------------------------------------------
CURRENT OBSERVATIONS

1. The C++ version works fine, or rather, I've yet to see it break.

2. Comparing the _cli and _cpp serialized files always demonstrates that they're the same. 

3. Step 4 never fails. Apparently when deserialization does work, it seems to work correctly.

4. The result of step 5 depend on whether step 2 is performed. Specifically, in the CLR version of the application, abberant behavior is observed under
the following conditions
	a. Step 2 is carried out. That is, the map object is loaded into class Foo. 
	b. The larger directory is tested. No issues seem to ever arise from deserializing /testdata/64/
One of the following things appears to take place somewhat consistently. 
	a. If the serialization step is commented out, the first attempt to deserialize correctly results in an std::map with 16 files. Step 5 verifies their 
	contents. Every subsequent attempt only manages to load 12 of the 16 files. 
	b. The application crashes on serialization. Debugging this is a total bitch. It appears to crash consistently when serializing the larger directory 
	in the second iteration.
	
	
As mentioned before, the C++ version of the application does not appear to share this issue
	
	
	
	