#ifndef SLIB_H_
#define SLIB_H_

#include <string>
#include <map>
#include <vector>

bool serializeDirectory(std::string folder, std::string output);
std::map<std::string, std::string> deserializeDirectory(std::string filepath);
bool verifyDirectory(std::string folder, std::map<std::string, std::string> &filemap, std::vector<std::string> &errants);

class Foo {
public:
	int load(std::string filepath);
private:
	std::map<std::string, std::string> _filemap;
};

#endif