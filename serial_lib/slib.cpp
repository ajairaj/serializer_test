#include "slib.h"

#include <Windows.h>
#include <iostream>
#include <fstream>
#include <sstream>
#include <map>
#include <string>

#include <boost/archive/binary_oarchive.hpp> 
#include <boost/archive/binary_iarchive.hpp>
#include <boost/serialization/map.hpp> 
#include <boost/serialization/string.hpp> 

using namespace std;

string strrep(string input, string substrOld, string substrNew) {
	size_t index = 0;
	while (true) {
		/* Locate the substring to replace. */
		index = input.find(substrOld, index);
		if (index == string::npos) break;
		/* Make the replacement. */
		input.replace(index, substrOld.size(), substrNew);
		index += substrNew.size();
	}
	return input;
}

string combine(vector<string> fileparts) {
	string out;
	for (unsigned int i = 0; i < size(fileparts); i++) {
		out += fileparts[i];
		if ((i < size(fileparts) - 1) && (out.back() != '/') && (out.back() != '\\')) {
			out += "/";
		}
	}
	out = strrep(out, "\\", "/");
	out = strrep(out, "//", "/");
	return out;
}

bool serializeDirectory(string folder, string output) {
	// find all of the files in config folder
	vector<string> names;
	string search_path = folder + "/*.*";
	WIN32_FIND_DATA fd;
	HANDLE hFind = ::FindFirstFile(search_path.c_str(), &fd);
	if (hFind != INVALID_HANDLE_VALUE) {
		do {
			// read all (real) files in current folder
			// , delete '!' read other 2 default folder . and ..
			if (!(fd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)) {
				names.push_back(fd.cFileName);
			}
		} while (::FindNextFile(hFind, &fd));
		::FindClose(hFind);
	}

	// save file contents to stl map
	//cout << "found " << names.size() << " files to serialize" << endl;
	map<string, string> filemap;
	for (int i = 0; i < names.size(); i++) {
		if (output == names[i])
			continue;
		else if ((names[i].substr(names[i].find_last_of(".") + 1) == "srl"))
			continue;
		string filepath = combine(vector<string>({ folder, names[i] }));
		ifstream ifs(filepath, ios::in | ios::binary);

		string content((istreambuf_iterator<char>(ifs)), (istreambuf_iterator<char>()));
		filemap[names[i]] = content;

		ifs.close();
	}
	//cout << "finished reading files" << endl;

	// serialize map with the power of boost
	stringstream ss;
	boost::archive::binary_oarchive oa(ss);
	try {
		oa << filemap;
	}
	catch (const exception& e) {
		cout << e.what() << endl;
		return false;
	}

	string content = ss.str();

	// save file
	string filepath = combine(vector<string>({ folder, output }));
	ofstream ofs(filepath, ios::out | ios::binary);
	ofs << content;
	ofs.close();

	//cout << "finished" << endl;
	return true;
}

map<string, string> deserializeDirectory(string filepath) {
	map<string, string> filemap;
	// read file
	ifstream ifs(filepath, ios::in | ios::binary);
	if (!ifs.good()) {
		cout << "failed to read file" << endl;
		return filemap;
	}

	string content((istreambuf_iterator<char>(ifs)), (istreambuf_iterator<char>()));

	// deserialize
	stringstream ss;
	ss << content;

	boost::archive::binary_iarchive ia(ss);

	try {
		ia >> filemap;
	}
	catch (const exception& e) {
		cout << e.what() << endl;
	}
	return filemap;
}

bool verifyDirectory(string folder, map<string, string> &filemap, vector<string> &errants) {
	bool good = true;
	for (map<string, string>::iterator it = filemap.begin(); it != filemap.end(); it++) {
		string filename = it->first;
		string mapData = it->second;

		vector<char> mapBytes(mapData.begin(), mapData.end());

		// read data from file
		string filepath = combine(vector<string>({ folder, filename }));
		ifstream ifs(filepath, ios::in | ios::binary);
		char ch;
		int counter = 0;
		while (ifs.get(ch)) {
			if (ch != mapBytes[counter]) {
				errants.push_back(filename);
				good = false;
			}
			counter++;
		}
		if (counter != mapData.length()) {
			errants.push_back(filename);
			good = false;
		}

		ifs.close();
	}
	return good;
}

int Foo::load(std::string filepath) {
	_filemap = deserializeDirectory(filepath);
	return _filemap.size();
}