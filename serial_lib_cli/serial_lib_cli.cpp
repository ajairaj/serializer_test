// This is the main DLL file.

#include <msclr\marshal_cppstd.h>
#include <string>

#include "serial_lib_cli.h"
#include "../serial_lib/slib.h"

namespace serial_lib_cli {

	Foo_CLI::Foo_CLI() {
		_foo = new Foo();
	}

	int Foo_CLI::load(System::String ^file) {
		int count = _foo->load(msclr::interop::marshal_as<std::string>(file));
		return count;
	}

	bool Foo_CLI::serializeDirectory_CLI(System::String ^folder, System::String ^output) {
		bool success = serializeDirectory(msclr::interop::marshal_as<std::string>(folder),
			msclr::interop::marshal_as<std::string>(output));
		return success;
	}

	int Foo_CLI::deserializeDirectory_CLI(System::String ^file) {
		std::map<std::string, std::string> filemap;
		filemap = deserializeDirectory(msclr::interop::marshal_as<std::string>(file));
		return filemap.size();
	}
	int Foo_CLI::verifyDirectory_CLI(System::String ^folder, System::String ^file) {
		std::map<std::string, std::string> filemap;
		filemap = deserializeDirectory(msclr::interop::marshal_as<std::string>(file));
		if (filemap.size() == 0) {
			return -1;
		}

		for (std::map<std::string, std::string>::iterator it = filemap.begin(); it != filemap.end(); it++) {
			std::string filename = it->first;
			std::string mapData = it->second;
		}

		std::vector<std::string> errants;
		bool verified = verifyDirectory(msclr::interop::marshal_as<std::string>(folder), filemap, errants);
		if (!verified) {
			return -2;
		}
		return filemap.size();
	}
}