// serial_lib_cli.h
#include "../serial_lib/slib.h"

#pragma once

using namespace System;

namespace serial_lib_cli {
	public ref class Foo_CLI {
		Foo *_foo;
	public:
		Foo_CLI();
		int load(System::String ^file);
		bool serializeDirectory_CLI(System::String ^folder, System::String ^output);
		int deserializeDirectory_CLI(System::String ^file);
		int verifyDirectory_CLI(System::String ^folder, System::String ^file);
	};
	
}
