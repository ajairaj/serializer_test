﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Serializer_Test_CS
{
    class Program
    {
        static void Main(string[] args)
        {
            List<string> dataDirs = new List<string>();
            dataDirs.Add("../../../test_data/64");
            dataDirs.Add("../../../test_data/256");
            //dataDirs.Add("../../../test_data/cfg");
            
            for (int k = 0; k < 1000; k++)
            {
                Console.WriteLine("----------ITERATION " + k.ToString() + "----------");
                
                for (int i = 0; i < dataDirs.Count; i++)
                {
                    serial_lib_cli.Foo_CLI f = new serial_lib_cli.Foo_CLI();

                    Console.WriteLine("serializing directory " + i.ToString());
                    bool success = f.serializeDirectory_CLI(dataDirs[i], "out_cli.srl");
                    if (!success)
                    {
                        Console.WriteLine("uh oh");
                        Console.ReadKey();
                    }

                    // COMMENT THIS PART OUT TO (maybe) FIX BEHAVIOR ///////////////////////////////////////////////////////////////////
                    Console.WriteLine("loading directory " + i.ToString());
                    int count = f.load(dataDirs[i] + "/out_cli.srl");
                    Console.WriteLine("loaded " + count.ToString() + " files into memory");
                    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

                    int status = f.verifyDirectory_CLI(dataDirs[i], dataDirs[i] + "/out_cli.srl");
                    if(status == -1)
                    {
                        Console.WriteLine("OH NO: Failed to deserialize map");
                        Console.ReadKey();
                    } else if(status == -2)
                    {
                        Console.WriteLine("OH NO: Failed verification");
                        Console.ReadKey();
                    } else
                    {
                        Console.WriteLine("deserialized and verified " + status.ToString() + " files");
                    }

                }
            }
            Console.WriteLine("Done");
            Console.ReadKey();

        }
    }
}
