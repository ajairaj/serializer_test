#include <iostream>
#include <vector>
#include <string>

#include "../serial_lib/slib.h"

using namespace std;

int main() {
	vector<string> dataDirs = { "../test_data/64", "../test_data/256" };
	char u;
	for (int k = 0; k < 1000; k++) {
		cout << "----------ITERATION " << k << "----------" << endl;

		for (int i = 0; i < dataDirs.size(); i++) {

			cout << "serializing directory " << i << endl;
			bool success = serializeDirectory(dataDirs[i], "out_cpp.srl");
			if (!success) {
				cout << "uh oh" << endl;
				cin >> u;
			}

			cout << "loading directory " << i << endl;
			Foo f;
			int count = f.load(dataDirs[i] + "/out_cpp.srl");
			cout << "loaded " << count << " files into memory" << endl;

			map<string, string> filemap;
			filemap = deserializeDirectory(dataDirs[i] + "/out_cpp.srl");
			if (filemap.size() == 0) {
				cout << "OH NO: Failed to deserialize map" << endl;
				cin >> u;
			}
			else {
				vector<string> errants;
				int status = verifyDirectory(dataDirs[i], filemap, errants);
				if (status == -1) {
					cout << "OH NO: Failed verification! The following files are $(%#ed: " << endl;
					for (int e = 0; e < errants.size(); e++) {
						cout << "    " << errants[e] << endl;
					}
					cin >> u;
				}
				else {
					cout << "deserialized and verified " << filemap.size() << " files" << endl;
				}
			}
		}
	}
	cout << "done" << endl;
	cin >> u;
	return 0;
}